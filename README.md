Coding Guidelines
--
1. Pick a [Git Branching Model](#5) for your project. Or understand the branching model already in use for your project.
2. Inside the issue tracker, Identify the piece of requirement you want to code (it should have label of `Epic` or `Story`).
3. [Create a `Task` issue under the `Epic` or `Story` issue](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements/).
4. [Propose an Architecture](https://gitlab.com/smarter-codes/guidelines/software-engineering/architecture-design/architecting), ensure it is reviewed by 1-2 colleagues in our review tool (gitlab). Verbal Reviews do not count
5. Now you are ready to code! While you code every few hours, [Comment in issue tracker and embrace Open Approach](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements/-/issues/1#the-how)
6. [Create Merge Request using the issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue). This will create a new branch. We will call this [Feature branch](https://nvie.com/posts/a-successful-git-branching-model/)<sup>`IMPORTANT: Watch this video for 30m`</sup>.
7. [Pick a Reviewer](https://gitlab.com/smarter-codes/guidelines/software-engineering/code-review/-/issues/11) for your MR. Only a reviewer will merge your feature branch into either [develop, release, hotfix or master branch](https://nvie.com/posts/a-successful-git-branching-model/).
8. [CICD](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy/cicd) will deploy the merged branch on a server (no human). If CICD does not exists, get in touch with DevOps in Engineering team

# Style Guide <sup>Please contribute by linking to styleguides</sup>
* Python
* Javascript
* React.js
* Vue.js
